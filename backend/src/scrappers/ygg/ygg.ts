import FormData from 'form-data';
import fs from 'fs';
import { mkdir } from 'fs/promises';
import got from 'got';
import fetch from 'node-fetch';
import path from 'path';
import {AllDebrid} from '../../debriders/alldebrid/alldebrid-debrider';
import {Debrider} from '../../debriders/debrider';
import {MediaInfos} from '../../entities/media-infos';
import {ScrapperTorrentInfos} from '../../entities/scrapper-torrent-infos';
import {User} from '../../entities/user';
import {FlareSolver, TorrentFlareSolver, YggBypassData} from '../../services/flare-solver';
import {SSH} from '../../storage/ssh/ssh';
import {Storage} from '../../storage/storage';
import {Uptobox} from '../../storage/uptobox/uptobox';
import {Torrent} from './torrent';
import {TorrentProviderEnum} from './torrent-provider-enum';
import {TorrentsList} from './torrents-list';
import parseTorrent from 'parse-torrent';

const cheerio = require('cheerio');

const getTorrentsApi = async (title: string): Promise<Torrent[]> => {
    // const searchTorrentApiTorrents = await search(title, 'Movies', 20);

    const searchTorrentApiTorrents = await FlareSolver.getTorrents(`https://www.ygg.re/engine/search?name=${title}&do=search`)

    return searchTorrentApiTorrents.map((searchTorrentApiTorrent: TorrentFlareSolver) => {
        return new Torrent(
          TorrentProviderEnum.YGG,
          searchTorrentApiTorrent.title,
          // TODO: change definition file for this lib
          //@ts-ignore
          searchTorrentApiTorrent.url,
          +searchTorrentApiTorrent.size,
            searchTorrentApiTorrent.seed,
            searchTorrentApiTorrent.leach,
          // searchTorrentApiTorrent.time,
          // searchTorrentApiTorrent.magnet,
          // searchTorrentApiTorrent.desc
        );
    })
}

// TODO: extract user part
/**
 * Returns a list of Ygg torrents for a particular title
 * @param title
 * @returns {Promise<Array>}
 */
export const getTorrentsList = async (title: string) => {

    const torrents = await getTorrentsApi(title);

    return new TorrentsList(TorrentProviderEnum.YGG, torrents);
};

const login = async (user: string, password: string, byPassData: YggBypassData) => {

    try {

        const formData = new FormData();
        formData.append('id', user);
        formData.append('pass', password);

        // Using fetch
        const response = await fetch('https://www.ygg.re/auth/process_login',
          {
              method: 'post',
              body: JSON.stringify({
                  id: 'Ghyslain',
                  pass: 'foobar'
              }),
              headers: {
                  'Cookie': byPassData.cookies,
                  'User-Agent': byPassData.userAgent,
                  'Content-Type': '!ç'
              }
          }
          );
        const data = await response.text();

        return data;

    } catch(error) {
        console.error(error);
        throw new Error(`Unable to login to YGG private tracker`);
    }
}

const getTorrentPage = async (torrentPageUrl: string, byPassData: YggBypassData): Promise<string> => {

    try {
        const response = await got(torrentPageUrl, {
            headers: {
                'Cookie': byPassData.cookies.map(cookie => `${cookie.name}=${cookie.value};`).join(''),
                // 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0'
                'User-Agent': byPassData.userAgent
            }
        });

        return response.body;
    } catch(error) {
        console.log(`YGG bypass data : ${JSON.stringify(byPassData)}`)
        console.error(error);
        throw new Error('Unable to get YGG torrent page');
    }

}

const downloadTorrent = async (torrentUrl: string, torrentTempPath: string, byPassData: YggBypassData) => {

    try {

        const torrentDownloaded = await got(torrentUrl, {
            headers: {
                'Cookie': byPassData.cookies.map(cookie => `${cookie.name}=${cookie.value};`).join(''),
                'User-Agent': byPassData.userAgent
            },
            // This is very important, otherwise data is encoded and cannot be properly used
            encoding: null
        });

        const dirCreation = await mkdir(path.join(__dirname, '/torrent_temp'), { recursive: true });

        console.log(dirCreation);

        fs.writeFileSync(torrentTempPath, torrentDownloaded.body);


    } catch(error) {
        console.log(`YGG bypass data : ${JSON.stringify(byPassData)}`)
        console.error(error);
        throw new Error('Unable to download YGG torrent');
    }
}

/**
 * Start the download of a torrent file -> to magnetLink -> deletes torrent file -> add to realdebrid service
 * @param user
 * @param scrapperTorrentInfos
 * @param mediaInfos
 */
export const downloadTorrentFile = async (user: User, scrapperTorrentInfos: ScrapperTorrentInfos, mediaInfos: MediaInfos) => {

    // Step 1 - Bypass CF
    const byPassData = await FlareSolver.getYggBypassData();

    // Step 2 - Login to YGG
    // await login('Ghyslain', 'foobar', byPassData);

    // Step 3 - Grab torrent page html infos
    const torrentPageHtml = await getTorrentPage(scrapperTorrentInfos.url, byPassData);

    // Scrap useful info from website - move this code into proper function
    const $ = cheerio.load(torrentPageHtml);
    const downloadTorrentUrl = $('div[id="informationsContainer"] table tr a').attr('href');
    const torrentTempPath = path.join(__dirname, '/torrent_temp/file.torrent');

    await downloadTorrent(downloadTorrentUrl, torrentTempPath, byPassData);

    const torrentSavedInAFile = parseTorrent(fs.readFileSync(torrentTempPath));

    await FlareSolver.removeAllFiles(path.join(__dirname, 'torrent_temp'));

    // Create magnet link using torrent file infos
    const magnetLink = parseTorrent.toMagnetURI(
      torrentSavedInAFile
    );

    // TODO: think about it to improve !
    const debrider = new Debrider(AllDebrid, user);
    const storage = new Storage(SSH, AllDebrid, user);

    // TODO: create a new storage called SSH that will implement all IStorage functions
    // const storage = new Storage(AllDebrid, SSH, user);

    console.log("BEFORE ADDING MAGNET");
    const torrentInfos = await debrider.addMagnet(magnetLink, mediaInfos, user);
    console.log("AFTER ADDING MAGNET");
    console.log("---------");
    console.log("ADD FILE TO STORAGE");
    await storage.addTorrent(mediaInfos, torrentInfos, user);
    // await storage.addTorrent(mediaInfos, torrentInfos, user);
    console.log("AFTER ADDING TORRENT");
}

module.exports.getTorrentsList = getTorrentsList;
module.exports.downloadTorrentFile = downloadTorrentFile;
