import fs from 'fs';
import got from 'got';
import parseTorrent from 'parse-torrent';
import path from 'path';
import {ScrapperTorrentInfos} from '../entities/scrapper-torrent-infos';
import FormData from 'form-data'
const cheerio = require('cheerio');

type FlareSolverResponse = {
    status: string,
    message: string,
    startTimestamp: number,
    endTimestamp: number,
    version: string,
    solution: {
        url: string,
        status: number,
        headers: {
            //TODO
        },
        response: string,
        cookies: any[]
    },
    //TODO
}

export type TorrentFlareSolver = {
    url: string;
    title: string;
    size: string;
    seed: number;
    leach: number
}

export type GenericCookie = {
    name: string;
    value: string;
}

export type YggBypassData = {
    cookies: GenericCookie[],
    userAgent: string
}

type CloudFlareUsefulCookies = {
    name: string
    value: string
}

type YggAuthenticationCookie = {
    version: number,
    name: string,
    value: string,

    port: any, // ?
    port_specified: boolean,
    domain: string,
    domain_specified: boolean,
    domain_initial_dot: boolean,
    path: string,
    path_specified: boolean,
    secure: boolean,
    expires: number,
    discard: boolean,
    comment: any, // ?
    comment_url: any, // ?
    rfc2109: boolean,
    _rest: any // ?
}

type YggAuthenticationSession = {
    url: string,
    status: number,
    response: string,
    headers: any,
    cookies: YggAuthenticationCookie[],
    userAgent: string
}

export class FlareSolver {

    static solverUrl: string = 'http://flaresolver:8191/v1';

    static cookies: GenericCookie[] = [];

    static userAgent = '';

    /**
     * TODO: move this code to ygg provider section
     * @param user
     * @param password
     */
    static async authenticateYGG(user: string, password: string): Promise<YggAuthenticationCookie[]> {
        try {
            const response: any = await got.post(FlareSolver.solverUrl, {
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },
                body: JSON.stringify({
                    cmd: "request.post",
                    url: "https://www.ygg.re/auth/process_login",
                    maxTimeout: 60000,
                    postData: `id=${user}&pass=${password}`,
                })
            });

            return JSON.parse(response.body).solution.cookies;
        } catch (error) {
            console.error(`Error authentication ygg tracker: ${error}`);
        }
    }

    static async createSession(): Promise<string> {
        try {
            const response: any = await got.post(FlareSolver.solverUrl, {
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },
                body: JSON.stringify({
                    cmd: "sessions.create",
                    maxTimeout: 60000
                })
            });

            return JSON.parse(response.body).session;
        } catch (error) {
            console.error(`Error creating flare solver session`);
        }
    }

    static async authenticateYGGUsingSession(user: string, password: string, sessionUUID: string): Promise<YggAuthenticationSession> {
        try {

            const response: any = await got.post(FlareSolver.solverUrl, {
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },
                body: JSON.stringify({
                    cmd: "request.post",
                    url: "https://www.ygg.re/auth/process_login",
                    maxTimeout: 60000,
                    postData: `id=${user}&pass=${password}`,
                    session: `${sessionUUID}`
                })
            });

            return JSON.parse(response.body).solution;
        } catch (error) {
            console.error(`Error authentication ygg tracker using flare solver session: ${error}`);
        }
    }

    /**
     * Previous to this, authenticate to YGG tracker by now
     * Returning only html of the response
     * @param url
     * @param cookies
     */
    static async getWithCookies(url: string): Promise <YggAuthenticationSession> {
        try {

            const cookies = await FlareSolver.authenticateYGG("Ghyslain", "foobar");

            const response: any = await got.post(FlareSolver.solverUrl, {
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },
                body: JSON.stringify({
                    cmd: "request.get",
                    url: url,
                    maxTimeout: 60000,
                    cookies: cookies
                })
            });

            return JSON.parse(response.body).solution;
        } catch (error) {
            console.error(`Error getting torrents: ${error}`);
        }
    }

    static async getWithSession(url: string): Promise <string> {
        try {

            // If no useful session present :
            // Create session
            // Log in tracker within this session
            const sessionUUID = await FlareSolver.createSession();

            // Authenticating with
            const session = await FlareSolver.authenticateYGGUsingSession("Ghyslain", "foobar", sessionUUID);
            // If a working session is present
            // -> retrieve sessionUUID

            const response: any = await got.post(FlareSolver.solverUrl, {
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },
                body: JSON.stringify({
                    cmd: "request.get",
                    url: url,
                    maxTimeout: 60000,
                    session: sessionUUID
                })
            });

            return JSON.parse(response.body).solution.response;
        } catch (error) {
            console.error(`Error getting url ${url}: ${error}`);
        }
    }

    // TODO: use a proper typing, corresponding to type definition of flare-solver lib !
    // TODO: move this function into ygg provider code
    static async getTorrents(url: string): Promise<TorrentFlareSolver[]> {
        try {

            const session = await FlareSolver.getWithCookies(url);
            // const html = await FlareSolver.getWithSession(url);

            this.cookies = session.cookies;
            this.userAgent = session.userAgent;

            const $ = cheerio.load(session.response);

            return $('div[class="table-responsive results"] tbody tr').toArray().map((torrentHtml: any) => {
                return {
                    url: torrentHtml.children[3].children[0].attribs.href,
                    title: torrentHtml.children[3].children[0].children[0].data,
                    size: torrentHtml.children[11].children[0].data,
                    seed: +torrentHtml.children[15].children[0].data,
                    leach: +torrentHtml.children[17].children[0].data
                }
            })
        } catch(error) {
            console.log(error);
        }
    }

    static async getYggBypassData(): Promise<YggBypassData> {
        try {

            // Using flaresolver
            // const result = await got.post(FlareSolver.solverUrl, {
            //     headers: {
            //         'Content-Type': 'application/json; charset=UTF-8',
            //     },
            //     body: JSON.stringify({
            //         cmd: "request.get",
            //         url: 'https://www.ygg.re',
            //         maxTimeout: 60000
            //     })
            // })
            //
            // const solution = JSON.parse(result.body).solution;
            //
            // const cookies = solution.cookies.map((cookie: YggAuthenticationCookie) => {
            //     return {
            //         name: cookie.name,
            //         value: cookie.value,
            //     }
            // })
            //
            // let cookiesFormatted = '';
            //
            // cookies.map((cookie: CloudFlareUsefulCookies) => {
            //     cookiesFormatted = cookiesFormatted + cookie.name + '=' + cookie.value + ';'
            // });
            //
            // const userAgent = solution.userAgent;

            return {
                cookies: this.cookies,
                userAgent: this.userAgent
            };

            // Only using got
            // const formData = new FormData();
            // formData.append('id','Ghyslain');
            // formData.append('pass','foobar');
            // formData.append('ci_csrf_token','');
            //
            // const response = await got('https://www5.yggtorrent.fi/user/login', {
            //     method: 'POST',
            //     headers: formData.getHeaders(),
            //     body: formData
            // });
            //
            // return {
            //     // cookies: `${cookie.name}=${cookie.value}`,
            //     // cookies: cookie,
            //     cookie: response.headers['set-cookie'][0],
            //     // userAgent: JSON.parse(result.body).solution.userAgent
            //     userAgent: ''
            // }
        } catch(error) {
            console.log(error);
        }
    }

    /**
     * Removes all files of a particular directory (to use in torrent_temp directory)
     * @param directory
     * @returns {Promise<any>}
     */
    static async removeAllFiles(directory: any) {
        return new Promise<void>((resolve, reject) => {
            fs.readdir(directory, (err: any, files: any) => {
                if (err) reject(err);

                for (const file of files) {
                    fs.unlink(path.join(directory, file), (err: any) => {
                        if (err) reject(err);
                    });
                }
                resolve();
            });
        })
    };

    static async downloadAndReturnYGGTorrent(torrentInfos: ScrapperTorrentInfos): Promise<any> {
        try {

            // const torrentPageHtml = await FlareSolver.getWithCookies(torrentInfos.url);
            const byPassData = await FlareSolver.getYggBypassData();



            const $ = cheerio.load(torrentPageHtml);

            const downloadTorrentUrl = $('div[id="informationsContainer"] table tr a').attr('href');

            // const bypassData = await FlareSolver.getYggBypassData();

            const torrentTempPath = path.join(__dirname, '/torrent_temp/file.torrent');

            const testTorrent = await FlareSolver.getWithSession(downloadTorrentUrl);

            // const cookies = await FlareSolver.authenticateYGG("Ghyslain", "foobar");

            // const torrentDownloaded = await got(downloadTorrentUrl, {
            //         headers: {
            //             // TODO: should not use ts-ignore
            //             // @ts-ignore
            //             'Cookie': cookies,
            //             // 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0'
            //             'User-Agent': 'PostmanRuntime/7.42.0'
            //         },
            //         // This is very important, otherwise data is encoded and cannot be properly used
            //         encoding: null
            //     });

            fs.writeFileSync(torrentTempPath, torrentDownloaded.body);

            const torrentSavedInAFile = parseTorrent(fs.readFileSync(torrentTempPath));

            await FlareSolver.removeAllFiles(path.join(__dirname, 'torrent_temp'));

            return torrentSavedInAFile;

        } catch(error) {
            console.log(error);

            throw new Error(`Unable to bypass cloudflare to download torrent -> ${error}`);
        }
    }

}
