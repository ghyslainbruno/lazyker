import * as admin from 'firebase-admin';
import * as logger from '../logs/logger';
import {SSH} from '../storage/ssh/ssh';
import {Storage} from '../storage/storage';

module.exports = (app: any) => {
    /*
     * Delete a storage current download
     */
    app.delete('/api/storages/download', async (req: any, res: any) => {
        try {
            const { user } = req;
            const { downloadId } = req.query;
            await Storage.deleteCurrentDownload(user, downloadId);
            res.send({ message: 'ok' });
            // res.send(logs[0]);
        } catch(error) {
            res.status(500).send({message: error})
        }
    });

    app.get('/api/storages/ssh', async (req: any, res: any) => {
        try {

            const user = req.user;
            const config = user.settings.storages.ssh;
            const ssh = new SSH(config.username, config.password.toString(), config.host, config.port, config.moviesPath, config.showsPath);

            // const ssh = new SSH('ghyslain', '21021992', '192.168.1.12', 22, '/home/ghyslain/nas/movies', '/home/ghyslain/nas/shows');

            await ssh.connect();
            console.log('been in there');
            res.send({ message: 'ok' });
        } catch(error) {
            res.status(500).send({message: error})
        }
    })
}
