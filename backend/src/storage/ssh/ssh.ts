import {data} from 'cheerio/lib/api/attributes';
import * as console from 'node:console';
import SSHConfig from 'ssh2-promise/lib/sshConfig';
import {UnlockedLink} from '../../debriders/alldebrid/alldebrid-debrider';
import {MediaInfos} from '../../entities/media-infos';
import {TorrentInDebriderInfos} from '../../entities/torrent-in-debrider-infos';
import {User} from '../../entities/user';
import {IStorage} from '../i-storage';
import SSH2Promise from 'ssh2-promise';

export class SSH implements IStorage {

  moviesPath: string;
  showsPath: string;
  host: string;
  username: string;
  password: string;
  port: number;

  constructor(username: string, password: string, host: string, port: number, moviesPath: string, showsPath: string) {
    this.username = username.toString();
    this.password = password.toString();
    this.host = host.toString();
    this.port = port;

    this.moviesPath = moviesPath
    this.showsPath = showsPath
  }

  async connect() {

    try {
      const config = {
        host: this.host,
        username: this.username,
        password: this.password,
        port: this.port,
      }

      const ssh = new SSH2Promise(config as SSHConfig);
      const data = await ssh.exec("cd /home/ghyslain/nas/movies && sudo mkdir Avatar");

      console.log(data); //ubuntu

      await ssh.close();
    } catch(error) {
      console.log(error);

      // Not creating folder but entering in it
      if (error.includes('File exists')) {

      }

    }

  }

  async addMovieFiles(unlockedLinks: UnlockedLink[], mediaInfos: MediaInfos): Promise<void> {

    const config = {
      host: this.host,
      username: this.username,
      password: this.password,
      port: this.port,
    }

    const ssh = new SSH2Promise(config as SSHConfig);

    // Creating the string with all the files to be downloaded for a particular movie, before SSH connection
    const downloadStringArray: string[] = []

    unlockedLinks.map(unlockedLink => {
      downloadStringArray.push(`sudo wget -bc "${unlockedLink.link}"`);
    })

    const downloadString = downloadStringArray.join(' && ');
    //

    try {
      const data = await ssh.exec(`cd ${this.moviesPath} && sudo mkdir "${mediaInfos.title}" && cd "${mediaInfos.title}" && ${downloadString}`);
      await ssh.close();
    } catch(error) {
      // Not creating folder but entering in it
      if (error.includes('File exists')) {
        console.log('Folder already exists -> not creating it and cd in it');
        try {
          const data = await ssh.exec(`cd ${this.moviesPath} && cd "${mediaInfos.title}" && ${downloadString}`);
          await ssh.close();
        } catch(error) {
          throw new Error(`Error adding movie | SSH storage | already existing movies folder -> ${error.message}`);
        }
      }
    }
  }

  async addShowFiles(unlockedLinks: UnlockedLink[], mediaInfos: MediaInfos): Promise<void> {

    const config = {
      host: this.host,
      username: this.username,
      password: this.password,
      port: this.port,
    }

    const ssh = new SSH2Promise(config as SSHConfig);

    // Creating the string with all the files to be downloaded for a particular movie, before SSH connection
    const downloadStringArray: string[] = []

    unlockedLinks.map(unlockedLink => {
      downloadStringArray.push(`sudo wget -bc "${unlockedLink.link}"`);
    })

    const downloadString = downloadStringArray.join(' && ');
    //

    try {
      const data = await ssh.exec(`cd ${this.showsPath} && sudo mkdir "${mediaInfos.title}" && cd "${mediaInfos.title}" && ${downloadString}`);
      await ssh.close();
    } catch(error) {
      // Not creating folder but entering in it
      if (error.includes('File exists')) {
        console.log('Folder already exists -> not creating it and cd in it');
        try {
          const data = await ssh.exec(`cd ${this.showsPath} && cd "${mediaInfos.title}" && ${downloadString}`);
          await ssh.close();
        } catch(error) {
          throw new Error(`Error adding show episodes | SSH storage | already existing show folder -> ${error.message}`);
        }
      }
    }

  }

  addTorrent(mediaInfos: MediaInfos, torrentInfos: TorrentInDebriderInfos, user: any): Promise<any> {
    // TODO
  }

}
