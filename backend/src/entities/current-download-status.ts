import {CurrentDownloadStatusEnum} from './current-download-status-enum';

export class CurrentDownloadStatus {
    status: CurrentDownloadStatusEnum;
    size: number;
    sizeDownloaded: number;

    constructor(status: CurrentDownloadStatusEnum, size: number, sizeDownloaded: number) {
        this.status = status;
        this.size = size;
        this.sizeDownloaded = sizeDownloaded;
    }
}
