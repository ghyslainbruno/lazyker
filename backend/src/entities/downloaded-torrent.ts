import {CurrentDownloadStatus} from './current-download-status';
import {MediaInfos} from './media-infos';
import {TorrentInDebriderInfos} from './torrent-in-debrider-infos';

export class DownloadedTorrent {
    id: number;
    mediaInfos: MediaInfos;
    torrent: TorrentInDebriderInfos;
    downloadStatus: CurrentDownloadStatus;

    constructor(mediaInfos: MediaInfos, torrent: TorrentInDebriderInfos, downloadStatus: CurrentDownloadStatus) {
        this.id = torrent.id;
        this.mediaInfos = mediaInfos;
        this.torrent = torrent;
        this.downloadStatus = downloadStatus;
    }
}
