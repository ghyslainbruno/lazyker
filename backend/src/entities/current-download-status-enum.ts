export enum CurrentDownloadStatusEnum {
    DOWNLOADING = 'downloading',
    ERROR = 'error',
    WAITING = 'waiting',
    FINISHED = 'finished',
    EXTRACTING = 'extracting',
    PAUSED = 'paused',
    FINISHING = 'finishing'
}
