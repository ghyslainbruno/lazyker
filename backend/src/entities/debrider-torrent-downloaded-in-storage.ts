import {TorrentStatusEnum} from './torrent-status.enum';


export type DebriderTorrentDownloadedInStorage = {
    downloadStatus: {
        size: number;
        sizeDownloaded: number;
        status: TorrentStatusEnum;
    },
    id: number;
    mediaInfos: {
        isShow: boolean;
        movieId: number;
        title: string;
        // TODO: should be of type "Year"
        year: number;
    },
    torrent: {
        id: number;
        isReady: boolean;
    }
}
