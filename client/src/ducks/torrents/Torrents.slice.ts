import {createAsyncThunk, createSelector, createSlice} from '@reduxjs/toolkit';
import firebase from 'firebase';
import ky from 'ky';
import {Dispatch} from 'redux';
import {auth} from '../../firebase';
import {Database} from '../../firebase/Database';
import {StorageEnum} from '../storages/Storage.enum';
import {setCurrentDownloadsLoading, updateCurrentDownloads} from '../storages/Uptobox.slice';
import {TorrentStatusEnum} from './TorrentStatus.enum';

export type DebriderTorrentDto = {
    filename: string;
    // TODO: change that to an enum
    status: string;
    // Maybe use a value object for this property
    id: string;
    // Between 0 to 100
    progress: number;
}

type TorrentSliceInitialState = {
    torrents: DebriderTorrentDto[];
    loading: boolean;
}

export const listenDebriderTorrents = async (dispatch: Dispatch<any>) => {

    // const storageSelected = useSelector(getStorageSelected);
    dispatch(setCurrentDownloadsLoading(true));

    firebase
        .database()
        .ref('/users')
        .child(await auth.getUid())
        .child(`/torrentsDownloadedInDebrider/${StorageEnum.UPTOBOX}`)
        .on('value', (snapshot: any) => {
            if (snapshot.val() !== null) {
                dispatch(updateCurrentDownloads(snapshot.val()));
            } else {
                dispatch(updateCurrentDownloads([]));
            }
            dispatch(setCurrentDownloadsLoading(false));
        })
}

export const fetchDebriderTorrents = createAsyncThunk(
    'debrider/fetchDebriderTorrents',
    async (): Promise<DebriderTorrentDto[]> => {
        return await ky.get(`/api/debrider/torrents`, {headers: {token: await auth.getIdToken()}}).json();
    }
)

export const deleteDebriderTorrent = createAsyncThunk(
    'debrider/deleteDebriderTorrent',
    async (torrent: DebriderTorrentDto | null): Promise<DebriderTorrentDto[]> => {
        return await ky.delete(`/api/debrider/torrent?torrentId=${torrent?.id}`, {headers: {token: await auth.getIdToken()}}).json();
    }
)

export const torrentsSlice = createSlice({
    name: 'torrents',
    initialState: {
        torrents: [],
        loading: false,
    } as TorrentSliceInitialState,
    reducers: {
        setDebriderTorrentsLoading: (state, action) => {
            state.loading = action.payload;
        }
    },
    extraReducers: {
        [fetchDebriderTorrents.fulfilled.type]: (state, action) => {
            state.torrents = action.payload;
            state.loading = false;
        },
        [fetchDebriderTorrents.pending.type]: (state, action) => {
            state.torrents = [];
            state.loading = true;
        },
        [fetchDebriderTorrents.rejected.type]: (state, action) => {
            state.torrents = [];
            state.loading = false;
        },

        [deleteDebriderTorrent.fulfilled.type]: (state, action) => {
            state.loading = false;
        },
        [deleteDebriderTorrent.pending.type]: (state, action) => {
            state.loading = true;
        },
        [deleteDebriderTorrent.rejected.type]: (state, action) => {
            state.loading = false;
        },
    }
})

export const { setDebriderTorrentsLoading } = torrentsSlice.actions

const getSlice = (state: any) => state.torrents.main;

export const getDebriderTorrents = createSelector([getSlice], state => state.torrents);
export const getDebriderTorrentsLoading = createSelector([getSlice], state => state.loading);

export default torrentsSlice.reducer
