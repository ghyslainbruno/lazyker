import {createAsyncThunk, createSelector, createSlice} from '@reduxjs/toolkit';
import firebase from 'firebase/app';
import 'firebase/database';
import ky from 'ky';
import {Dispatch} from 'redux';
import {auth} from '../../firebase';
import {displaySuccessNotification} from '../snack/Snackbar.slice';
import {DebriderTorrentDto} from '../torrents/Torrents.slice';
import {TorrentStatusEnum} from '../torrents/TorrentStatus.enum';
import {StorageEnum} from './Storage.enum';


export const fetchDebriderTorrents = createAsyncThunk(
    'debrider/fetchDebriderTorrents',
    async (): Promise<DebriderTorrentDto[]> => {
      return await ky.get(`/api/debrider/torrents`, {headers: {token: await auth.getIdToken()}}).json();
    }
)

export type DebriderTorrentDownloadedInStorage = {
  downloadStatus: {
    size: number;
    sizeDownloaded: number;
    status: TorrentStatusEnum;
  },
  id: number;
  mediaInfos: {
    isShow: boolean;
    movieId: number;
    title: string;
    // TODO: should be of type "Year"
    year: number;
  },
  torrent: {
    id: number;
    isReady: boolean;
  }
}

type StoragesInitialState = {
  storageSelected: StorageEnum,
  downloads: DebriderTorrentDownloadedInStorage[],
  downloadsLoading: boolean
}

export const deleteStorageDownload = createAsyncThunk("storages/deleteStorageDownload", async (currentDownloadToDelete: DebriderTorrentDownloadedInStorage, thunkAPI) => {
  return await ky.delete(`/api/storages/download?downloadId=${currentDownloadToDelete.id}`, {headers: {token: await auth.getIdToken()}}).json();
  thunkAPI.dispatch(displaySuccessNotification('Download deleted'));
});

export const saveStorage = createAsyncThunk("storages/saveStorage", async (state: any, thunkAPI) => {
  await firebase
    .database()
    .ref('/users')
    .child(await auth.getUid())
    .child('/settings/storages/selected')
    .set(state);

  thunkAPI.dispatch(displaySuccessNotification('Storage changed'));

  return state;
});

export const fetchStorage = createAsyncThunk("storages/fetchStorage", async () => {
  const snapshot = await firebase
    .database()
    .ref('/users')
    .child(await auth.getUid())
    .child('/settings/storages/selected')
    .once('value')

  return await snapshot.val();
});

export const listenStorageDownloads = async (dispatch: Dispatch<any>, selectedStorage: StorageEnum) => {

  dispatch(setDownloadsLoading(true));

  if (selectedStorage !== StorageEnum.NONE) {
    firebase
        .database()
        .ref('/users')
        .child(await auth.getUid())
        .child(`/torrentsDownloadedInStorage/${selectedStorage}`)
        .on('value', (snapshot: any) => {
          if (snapshot.val() !== null) {
            dispatch(updateDownloads(snapshot.val()));
          } else {
            dispatch(updateDownloads([]));
          }
          dispatch(setDownloadsLoading(false));
        })
  }
}

export const storageSlice = createSlice({
  name: 'storages',
  initialState: {
    storageSelected: StorageEnum.NONE,
    downloads: [],
    downloadsLoading: false
  } as StoragesInitialState,
  reducers: {
    updateStorage: (state, action) => {
      state.storageSelected = action.payload
    },
    updateDownloads: (state, action) => {
      state.downloads = Object.keys(action.payload).map(torrentId => action.payload[torrentId]) as []
    },
    setDownloadsLoading: (state, action) => {
      state.downloadsLoading = action.payload
    }
  },
  extraReducers: {
    [saveStorage.fulfilled.type]: (state, action) => {
      state.storageSelected = action.payload;
    },
    [saveStorage.pending.type]: (state, action) => {
      // state.storageSelected = StorageEnum.NONE;
    },
    [saveStorage.rejected.type]: (state, action) => {
      state.storageSelected = StorageEnum.NONE;
    },

    [fetchStorage.fulfilled.type]: (state, action) => {
      state.storageSelected = action.payload;
    },
    [fetchStorage.pending.type]: (state, action) => {
      state.storageSelected = StorageEnum.NONE;
    },
    [fetchStorage.rejected.type]: (state, action) => {
      state.storageSelected = StorageEnum.NONE;
    },
  }
})

// Does it work ??
const getSlice = (state: any) => state.storages;

export const { updateStorage, setDownloadsLoading, updateDownloads } = storageSlice.actions

export const getStorageSelected = createSelector([getSlice], state => state.main.storageSelected);
export const getStorageDownloads = createSelector([getSlice], state => state.main.downloads);
export const getStorageDownloadsLoading = createSelector([getSlice], state => state.main.loading);

export default storageSlice.reducer
