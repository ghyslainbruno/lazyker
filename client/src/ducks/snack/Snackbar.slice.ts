import {createSlice} from '@reduxjs/toolkit';
import {SeverityEnum} from './Severity.enum';

export const snackbarSlice = createSlice({
  name: 'snackbar',
  initialState: {
    opened: false,
    message: '',
    severity: SeverityEnum.NONE,
    isComingSoon: false,
  },
  reducers: {
    displayComingSoonNotification: (state, action) => {
      state.opened = true;
      state.severity = SeverityEnum.WARNING;
      state.message = action.payload;
      state.isComingSoon = true;
    },
    displaySuccessNotification: (state, action) => {
      state.opened = true;
      state.severity = SeverityEnum.SUCCESS;
      state.message = action.payload;
      state.isComingSoon = false;
    },
    displayErrorNotification: (state, action) => {
      state.opened = true;
      state.severity = SeverityEnum.ERROR;
      state.message = action.payload;
      state.isComingSoon = false;
    },
    closeSnackBar: (state, action) => {
      state.opened = false;
      state.isComingSoon = false;
    }
  },
})

// Action creators are generated for each case reducer function
export const { displayComingSoonNotification, displaySuccessNotification, closeSnackBar, displayErrorNotification } = snackbarSlice.actions

export default snackbarSlice.reducer
