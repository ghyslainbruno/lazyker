import React from 'react';
import Lottie from 'react-lottie';
import searchingAnimation from './99946-searching.json';
import lookingAnimation from './100948-looking-for.json';


const Searching = (props: any) => {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: lookingAnimation,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        }
    };

    return (
        <div>
            <Lottie
                options={defaultOptions}
                height={400}
                width={400}
            />
        </div>
    );
};
export default Searching;
