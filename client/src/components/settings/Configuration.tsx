import {Accordion, AccordionDetails} from '@material-ui/core';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// @ts-ignore
import queryString from 'qs';
import React, {useEffect, useState} from "react";
import {useDispatch} from 'react-redux';
import {isConnected} from '../../ducks/debriders/Alldebrid.slice';
import {displayErrorNotification} from '../../ducks/snack/Snackbar.slice';
import {auth} from '../../firebase';
import SignOutButton from '../../firebase/SignOutBtn';
import Debriders from './configuration/debriders';
import Storage from './configuration/storage';
import Logs from './logs';
import PrivacyPolicies from './privacy_policies';

let auth2: any = null;

type LinkRealdebridUserDto = {
    message: string;
}

type LoadSettingsDto = {
    settings: {
        storage: any;
        gdrive: {
            moviesGdriveFolder: {
                moviesGdriveFolderId: any;
                moviesGdriveFolderName: any;
                parentMoviesGdriveFolderId: any;
            };
            tvShowsGdriveFolder: {
                tvShowsGdriveFolderId: any;
                tvShowsGdriveFolderName: any;
                parentTvShowsGdriveFolderId: any;
            };
            token: any;
        };
        qualities: {
            first: any;
            second: any;
            third: any;
            h265: any;
        };
        nas: {
            moviesPath: any;
            tvShowsPath: any;
            protocol: any;
            host: any;
            port: any;
            account: any;
            password: any;
        };
        debriders: {
            alldebrid: {
                apiKey: string;
            }
        }
    }
}

type SetSettingsDto = {
    message: string;
}

type SettingsProps = {
    changeNavigation: (location: any) => void;
    location: {
        pathname: any;
        search: any;
    };
    history: any;
}

const Configuration = (props: SettingsProps) => {

    const dispatch = useDispatch()

    const [labelWidth, setLabelWidth] = useState(null);
    const [firstQuality, setFirstQuality] = useState('');
    const [secondQuality, setSecondQuality] = useState('');
    const [thirdQuality, setThirdQuality] = useState('');
    const [snack, setSnack] = useState(false);
    const [h265, seth265] = useState(false);
    const [loading, setLoading] = useState(false);
    const [moviesPath, setMoviesPath] = useState('');
    const [tvShowsPath, setTvShowsPath] = useState('');
    const [host, setHost] = useState('');
    const [port, setPort] = useState('');
    const [nasUsername, setNasUsername] = useState('');
    const [nasPassword, setNasPassword] = useState('');
    const [protocol, setProtocol] = useState('http');
    const [every, setEvery] = useState('');
    const [settingsLoading, setSettingsLoading] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [googleDriveConnectLoading, setGoogleDriveConnectLoading] = useState(false);
    const [storage, setStorage] = useState('');
    const [gdriveToken, setGdriveToken] = useState(null);
    const [moviesGdriveFolderId, setMoviesGdriveFolderId] = useState(null);
    const [moviesGdriveFolderName, setMoviesGdriveFolderName] = useState(null);
    const [parentMoviesGdriveFolderId, setParentMoviesGdriveFolderId] = useState(null);
    const [tvShowsGdriveFolderId, setTvShowsGdriveFolderId] = useState(null);
    const [tvShowsGdriveFolderName, setTvShowsGdriveFolderName] = useState(null);
    const [parentTvShowsGdriveFolderId, setParentTvShowsGdriveFolderId] = useState(null);
    const [snackBarMessage, setSnackBarMessage] = useState('');

    useEffect(() => {
        (async function mountingComponent() {
            props.changeNavigation('settings');

            gapi.load('auth2', function() {
                // @ts-ignore
                auth2 = gapi.auth2.init({
                    discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
                    clientId: '348584284-25m9u9qbgmapjd3vtt5oaai7mir5t7vu.apps.googleusercontent.com',
                    scope: 'https://www.googleapis.com/auth/drive'
                });
            });

            if (props.location !== undefined) {
                if (props.location.pathname === '/link_rd') {
                    const params = queryString.parse(props.location.search.replace(/^\?/,''));

                    try {
                        let response = await fetch('/api/link?code=' + params.code, {
                            method: 'GET',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'token': await auth.getIdToken()
                            }
                        });

                        const responseJSON: LinkRealdebridUserDto = await response.json();
                        setSnack(true);
                        setSnackBarMessage(responseJSON.message)
                    } catch(error) {
                        setSnack(true);
                        setSnackBarMessage('Error connecting to realdebrid')
                    }

                    props.history.push('/settings');
                }
            }
        })();
    }, []);

    const loadSettings = async () => {

        setSettingsLoading(true);

        try {

            const response = await fetch('/api/settings', {
                method: 'GET',
                headers: {
                    'token': await auth.getIdToken()
                },
            });
            const responseJSON: LoadSettingsDto = await response.json();
            const settings = responseJSON.settings;

            if (settings === null) {
                setSnack(true)
                setSnackBarMessage('Please configure lazyker')
                setSettingsLoading(false)
                setFirstQuality('')
                setSecondQuality('')
                setThirdQuality('')
                seth265(false)
                setMoviesPath('')
                setTvShowsPath('')
                setHost('')
                setPort('')
                setNasUsername('')
                setNasPassword('')
                setProtocol('http')
                setStorage('')
                setGdriveToken(null)
                setMoviesGdriveFolderId(null)
                setMoviesGdriveFolderName(null)
                setParentMoviesGdriveFolderId(null)
                setTvShowsGdriveFolderId(null)
                setTvShowsGdriveFolderName(null)
                setParentTvShowsGdriveFolderId(null)

            } else {

                if (settings.gdrive !== undefined) {
                    if (settings.gdrive.moviesGdriveFolder !== undefined) {
                        setMoviesGdriveFolderId(settings.gdrive.moviesGdriveFolder.moviesGdriveFolderId)
                        setMoviesGdriveFolderName(settings.gdrive.moviesGdriveFolder.moviesGdriveFolderName)
                        setParentMoviesGdriveFolderId(settings.gdrive.moviesGdriveFolder.parentMoviesGdriveFolderId)
                    } else {
                        setMoviesGdriveFolderId(null)
                        setMoviesGdriveFolderName(null)
                        setParentMoviesGdriveFolderId(null)
                    }

                    if (settings.gdrive.tvShowsGdriveFolder !== undefined) {
                        setTvShowsGdriveFolderId(settings.gdrive.tvShowsGdriveFolder.tvShowsGdriveFolderId)
                        setTvShowsGdriveFolderName(settings.gdrive.tvShowsGdriveFolder.tvShowsGdriveFolderName)
                        setParentTvShowsGdriveFolderId(settings.gdrive.tvShowsGdriveFolder.parentTvShowsGdriveFolderId)
                    } else {
                        setTvShowsGdriveFolderId(null)
                        setTvShowsGdriveFolderName(null)
                        setParentTvShowsGdriveFolderId(null)
                    }

                    if (settings.gdrive.token !== undefined) {
                        setGdriveToken(settings.gdrive.token)
                    } else {
                        setGdriveToken(null)
                    }

                } else {
                    setGdriveToken(null)
                }

                if (settings.qualities !== undefined) {
                    setFirstQuality(settings.qualities.first)
                    setSecondQuality(settings.qualities.second)
                    setThirdQuality(settings.qualities.third)
                    seth265(settings.qualities.h265)
                } else {
                    setFirstQuality('')
                    setSecondQuality('')
                    setThirdQuality('')
                    seth265(false)
                }

                if (settings.nas !== undefined) {
                    setMoviesPath(settings.nas.moviesPath)
                    setTvShowsPath(settings.nas.tvShowsPath)
                    setProtocol(settings.nas.protocol)
                    setHost(settings.nas.host)
                    setPort(settings.nas.port)
                    setNasUsername(settings.nas.account)
                    setNasPassword(settings.nas.password)
                } else {
                    setMoviesPath('')
                    setTvShowsPath('')
                    setProtocol('')
                    setHost('')
                    setPort('')
                    setNasUsername('')
                    setNasPassword('')
                }

                if (settings.hasOwnProperty('storage')) {
                    setStorage(settings.storage)
                } else {
                    setStorage('')
                }

                if (settings?.debriders?.alldebrid?.apiKey) {
                    dispatch(isConnected('connected'));
                } else {
                    dispatch(isConnected('disconnected'));
                }

                setSettingsLoading(false)
            }
        } catch(error) {
            setSnack(true);
            setSnackBarMessage('Error loading settings');
            setSettingsLoading(false);
        }
    };

    const setSettings = async () => {
        setSettingsLoading(true);
        try {
            let response = await fetch('/api/settings', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'token': await auth.getIdToken()
                },
                body: JSON.stringify({
                    qualities: {
                        first: firstQuality,
                        second: secondQuality,
                        third: thirdQuality,
                        h265: h265
                    },
                    nas: {
                        moviesPath: moviesPath,
                        tvShowsPath: tvShowsPath,
                        protocol: protocol,
                        host: host,
                        port: port,
                        account: nasUsername,
                        password: nasPassword
                    },
                    autoupdateTime: every,
                    storage: storage,
                    gdrive: {
                        moviesGdriveFolder: {
                            moviesGdriveFolderId: moviesGdriveFolderId,
                            moviesGdriveFolderName: moviesGdriveFolderName,
                            parentMoviesGdriveFolderId: parentMoviesGdriveFolderId,
                        },
                        tvShowsGdriveFolder: {
                            tvShowsGdriveFolderId: tvShowsGdriveFolderId,
                            tvShowsGdriveFolderName: tvShowsGdriveFolderName,
                            parentTvShowsGdriveFolderId: parentTvShowsGdriveFolderId,
                        }
                    }
                })
            });

            const responseJSON: SetSettingsDto = await response.json();

            setSnack(true);
            setSnackBarMessage(responseJSON.message);
            setSettingsLoading(false);
        } catch(error) {
            setSnack(true);
            setSnackBarMessage('Error settings settings');
            setSettingsLoading(false);
        }

    };

    const handlerQualityChange = (event: any) => {
        // @ts-ignore
        this.setState({ [event.target.name]: event.target.value });
    };

    const handleH265Change = (name: any) => (event: any) => {
        // @ts-ignore
        this.setState({ [name]: event.target.checked });
    };

    const googleDriveConnect = async () => {

        try {

            const code = await auth2.grantOfflineAccess();

            await fetch('/api/gdrive_auth', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'token': await auth.getIdToken()
                },
                body: JSON.stringify({
                    code: code
                })
            });

            await setSettings();

            await loadSettings();
        } catch (error) {
            dispatch(displayErrorNotification('Error connecting to Google Drive'))
            setGoogleDriveConnectLoading(false);
        }

    };

    const googleDriveDisConnect = async () => {
        try {
            let response = await fetch('/api/gdrive_disconect', {
                method: 'GET',
                headers: {
                    'token': await auth.getIdToken()
                }
            });
            response = await response.json();
            loadSettings();
        } catch(error) {
            setSnack(true);
            setSnackBarMessage('Error while unauthorizing lazker from Google Drive');
            setSettingsLoading(false);
        }
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleProtocolChange = (event: any) => {
        // @ts-ignore
        this.setState({ [event.target.name]: event.target.value });
    };

    const deleteMovieFolder = () => {
        setMoviesGdriveFolderId(null)
        setMoviesGdriveFolderName(null)
        setParentMoviesGdriveFolderId(null)
    };

    const setMovieFolder = (folder: any) => {
        setMoviesGdriveFolderId(folder.id)
        setMoviesGdriveFolderName(folder.name)
        setParentMoviesGdriveFolderId(folder.parentId)
    };

    const deleteShowsFolder = () => {
        setTvShowsGdriveFolderId(null)
        setTvShowsGdriveFolderName(null)
        setParentTvShowsGdriveFolderId(null)
    };

    const setShowsFolder = (folder: any) => {
        setTvShowsGdriveFolderId(folder.id)
        setTvShowsGdriveFolderName(folder.name)
        setParentTvShowsGdriveFolderId(folder.parentI)
    };

    const displaySnackMessage = (message: string) => {
        setSnack(true);
        setSnackBarMessage(message);
    };

    return (
        <div style={{width: '100%', marginBottom: '10vh'}}>

            <h1>Settings</h1>

            <div>
                <Accordion onChange={(event, expanded) => expanded ? loadSettings() : null}>
                    <AccordionSummary expandIcon={<ExpandMoreIcon/>}>
                        <Typography>Configuration</Typography>
                    </AccordionSummary>

                    {!settingsLoading ?
                        <div>

                            <Storage
                                googleDriveConnectLoading={googleDriveConnectLoading}
                                settingsLoading={settingsLoading}
                                moviesGdriveFolderName={moviesGdriveFolderName}
                                deleteMovieFolder={deleteMovieFolder}
                                setMovieFolder={setMovieFolder}
                                tvShowsGdriveFolderName={tvShowsGdriveFolderName}
                                deleteShowsFolder={deleteShowsFolder}
                                setShowsFolder={setShowsFolder}
                                gdriveToken={gdriveToken}
                                googleDriveDisConnect={googleDriveDisConnect}
                                googleDriveConnect={googleDriveConnect}
                                moviesPath={moviesPath}
                                setMoviesPath={setMoviesPath}
                                tvShowsPath={tvShowsPath}
                                setShowsPath={setTvShowsPath}
                                protocol={protocol}
                                handleProtocolChange={handleProtocolChange}
                                host={host}
                                setHost={setHost}
                                port={port}
                                setPort={setPort}
                                nasUsername={nasUsername}
                                setNasUserName={setNasUsername}
                                showPassword={showPassword}
                                nasPassword={nasPassword}
                                setNasPassword={setNasPassword}
                                handleClickShowPassword={handleClickShowPassword}
                            />

                            <Divider/>

                            <Debriders />

                        </div>
                        :
                        <CircularProgress style={settingsLoading ? {
                            display: 'inline-block',
                            margin: '5px'
                        } : {display: 'none'}}/>
                    }
                </Accordion>

                <Logs
                    displaySnackMessage={displaySnackMessage}
                />
            </div>

            <SignOutButton/>

            <PrivacyPolicies/>

        </div>
    );
}

export default Configuration;
