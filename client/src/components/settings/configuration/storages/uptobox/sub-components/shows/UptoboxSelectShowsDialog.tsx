import {Dialog, makeStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
    openMoviesDialog, openShowsDialog, saveMoviesFolder, saveShowsFolder,
} from '../../../../../../../ducks/storages/Uptobox.slice';
import {UptoboxFilesList} from '../movies/UptoboxFilesList';

const useStyles = makeStyles({
    root: {
        height: 240,
        flexGrow: 1,
        maxWidth: 400,
    },
});

type UptoboxSelectMovieDialogProps = {
    id: string;
    label: string;
}

export const UptoboxSelectShowsDialog = (props: UptoboxSelectMovieDialogProps) => {

    const isShowDialogOpened = useSelector((state: any) => state.storages.uptobox.isShowDialogOpened);
    const showsFolderPath = useSelector((state: any) => state.storages.uptobox.showsFolderPath);

    const dispatch = useDispatch();
    const classes = useStyles();

    const handleSaveClick = () => {
        dispatch(saveShowsFolder(showsFolderPath));
    }

    const handleDeleteClick = () => {
        dispatch(openShowsDialog(false));
    }

    return (
        <Dialog fullWidth={true} open={isShowDialogOpened} onClose={() => dispatch(openShowsDialog(false))}>
            <DialogTitle id="alert-dialog-title">Select shows folder : {showsFolderPath.replace('//','/')}</DialogTitle>

            <DialogContent>

                <UptoboxFilesList id={'//'} key={0} name="Uptobox folders" type="shows" />

            </DialogContent>

            <DialogActions>
                <Button onClick={handleDeleteClick} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleSaveClick} color="primary" autoFocus>
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    )
}
