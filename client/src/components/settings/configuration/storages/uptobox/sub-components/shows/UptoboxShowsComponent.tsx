import {makeStyles} from '@material-ui/core';
import React from "react"
import {UptoboxMoviesButton} from '../movies/UptoboxMoviesButton';
import {UptoboxMoviesState} from '../movies/UptoboxMoviesState';
import {UptoboxSelectMovieDialog} from '../movies/UptoboxSelectMovieDialog';
import {UptoboxSelectShowsDialog} from './UptoboxSelectShowsDialog';
import {UptoboxShowsButton} from './UptoboxShowsButton';
import {UptoboxShowsState} from './UptoboxShowsState';

const useStyles = makeStyles({
    container: {
        flex: 1,
        display: 'flex',
    },
    itemText: {
        flex: 1,
        alignSelf: 'center'
    },
})

export const UptoboxShowsComponent = () => {

    const classes = useStyles();

    return (
        <div style={{display: 'flex'}}>
            <UptoboxSelectShowsDialog id={'/'} label={'Uptobox folders'}/>

            <div className={classes.container}>
                <div className={classes.itemText}>
                    Shows
                </div>
            </div>

            <UptoboxShowsState/>

            <UptoboxShowsButton/>
        </div>
    )
}
