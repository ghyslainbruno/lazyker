import {AccordionDetails} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import {useDispatch} from 'react-redux';
import {isConnected} from '../ducks/debriders/Alldebrid.slice';
import {SeverityEnum} from '../ducks/snack/Severity.enum';
import { displayErrorNotification } from '../ducks/snack/Snackbar.slice';
import SignOutButton from '../firebase/SignOutBtn';
import { auth } from '../firebase';
// @ts-ignore
import queryString from 'qs';
import Logs from "./settings/logs";
import Qualities from "./settings/configuration/qualities";
import Storage from "./settings/configuration/storage";
import Debriders from "./settings/configuration/debriders";
import PrivacyPolicies from "./settings/privacy_policies";
import Save from "./settings/save";
import Configuration from './settings/Configuration';

let auth2: any = null;

type LinkRealdebridUserDto = {
    message: string;
}

type LoadSettingsDto = {
    settings: {
        storage: any;
        gdrive: {
            moviesGdriveFolder: {
                moviesGdriveFolderId: any;
                moviesGdriveFolderName: any;
                parentMoviesGdriveFolderId: any;
            };
            tvShowsGdriveFolder: {
                tvShowsGdriveFolderId: any;
                tvShowsGdriveFolderName: any;
                parentTvShowsGdriveFolderId: any;
            };
            token: any;
        };
        qualities: {
            first: any;
            second: any;
            third: any;
            h265: any;
        };
        nas: {
            moviesPath: any;
            tvShowsPath: any;
            protocol: any;
            host: any;
            port: any;
            account: any;
            password: any;
        };
        debriders: {
            alldebrid: {
                apiKey: string;
            }
        }
    }
}

type SetSettingsDto = {
    message: string;
}

type SettingsProps = {
    changeNavigation: (location: any) => void;
    location: {
        pathname: any;
        search: any;
    };
    history: any;
}

// TODO: extract it !!!
type Store = {
    snack: {
        opened: boolean;
        message: string;
        severity: SeverityEnum;
    }
}

const Settings = (props: SettingsProps) => {

    const [snack, setSnack] = useState(false);
    const [snackBarMessage, setSnackBarMessage] = useState('');

    useEffect(() => {
        (async function mountingComponent() {
            props.changeNavigation('settings');

            gapi.load('auth2', function() {
                // @ts-ignore
                auth2 = gapi.auth2.init({
                    discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
                    clientId: '348584284-25m9u9qbgmapjd3vtt5oaai7mir5t7vu.apps.googleusercontent.com',
                    scope: 'https://www.googleapis.com/auth/drive'
                });
            });

            if (props.location !== undefined) {
                if (props.location.pathname === '/link_rd') {
                    const params = queryString.parse(props.location.search.replace(/^\?/,''));

                    try {
                        let response = await fetch('/api/link?code=' + params.code, {
                            method: 'GET',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'token': await auth.getIdToken()
                            }
                        });

                        const responseJSON: LinkRealdebridUserDto = await response.json();
                        setSnack(true);
                        setSnackBarMessage(responseJSON.message)
                    } catch(error) {
                        setSnack(true);
                        setSnackBarMessage('Error connecting to realdebrid')
                    }

                    props.history.push('/settings');
                }
            }
        })();
    }, []);

    return (
        <Configuration {...props}/>
    )

}

export default Settings
