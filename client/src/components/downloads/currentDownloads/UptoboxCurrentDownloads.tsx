import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import LinearProgress from '@material-ui/core/LinearProgress';
import Download from '@material-ui/icons/GetApp';
import PauseFilled from '@material-ui/icons/PauseCircleFilled';
import PauseCircle from '@material-ui/icons/PauseCircleOutline';
import PlayCircle from '@material-ui/icons/PlayCircleOutline';
import RemoveCircle from '@material-ui/icons/RemoveCircleOutline';
import Delayed from '@material-ui/icons/WatchLater';
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
    DebriderTorrentDownloadedInStorage,
    deleteStorageDownload,
    getStorageDownloads,
    getStorageDownloadsLoading,
    getStorageSelected
} from '../../../ducks/storages/Storage.slice';
import {
    getUptoboxCurrentDownloads,
    getUptoboxCurrentDownloadsLoading,
    listenUptoboxCurrentDownloads
} from '../../../ducks/storages/Uptobox.slice';
import {DoneGreen, ErrorRed} from '../CurrentDownloads';

type UptoboxCurrentDownloadsProps = {

}

const UptoboxCurrentDownloads = (props: UptoboxCurrentDownloadsProps) => {

    const loading = useSelector(getStorageDownloadsLoading);
    // const currentDownloads = useSelector(getUptoboxCurrentDownloads);
    const downloads = useSelector(getStorageDownloads);
    const dispatch = useDispatch();


    useEffect(() => {
        // listenUptoboxCurrentDownloads(dispatch);
    }, []);

    const resumeDownload = (currentDownload: DebriderTorrentDownloadedInStorage) => {

    }

    const pauseDownload = (currentDownload: DebriderTorrentDownloadedInStorage) => {

    }

    const removeDownload = (downloadToDelete: DebriderTorrentDownloadedInStorage) => {
        dispatch(deleteStorageDownload(downloadToDelete));
    }

    return (
        <div>

            <CircularProgress style={loading ? {display: 'inline-block'} : {display: 'none'}} />

            {
                downloads.length === 0 ? <div style={{padding: '10px', fontSize: '0.9rem', color: 'grey'}}>no downloads</div> : null
            }

            {
                downloads.map((currentDownload: any, index: number) => {
                    return (
                        <div key={index}>
                            <div style={{display: 'flex', width: '100%', textAlign: 'left', padding: '5px', flexWrap: 'wrap', justifyContent: 'space-between'}}>

                                {/* Title */}
                                <div className="titleDownload">
                                    <p style={{fontSize: '0.9rem', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>{currentDownload.mediaInfos.title}</p>
                                </div>

                                <div className="actionDownload">
                                    {/* State icon */}
                                    <div style={{width: '8%', padding: '12px', textAlign: 'center'}}>
                                        {currentDownload.downloadStatus.status === 'downloading' ?
                                            <Download/>
                                            :
                                            currentDownload.downloadStatus.status === 'error' ?
                                                <ErrorRed/>
                                                :
                                                currentDownload.downloadStatus.status === 'waiting' ?
                                                    <Delayed/>
                                                    :
                                                    currentDownload.downloadStatus.status === 'finished' ?
                                                        <DoneGreen/>
                                                        :
                                                        currentDownload.downloadStatus.status === 'extracting' ?
                                                            <Download/>
                                                            :
                                                            currentDownload.downloadStatus.status === 'paused' ?
                                                                <PauseFilled/>
                                                                :
                                                                currentDownload.downloadStatus.status === 'finishing' ?
                                                                    <Download/>
                                                                    :
                                                                    null
                                        }
                                    </div>

                                    {/* Speed */}
                                    {/*<div style={{paddingLeft: '10px', paddingRight: '10px'}}>*/}
                                    {/*    <p>{currentDownload.downloadStatus.speed.toFixed(1).padStart(4, '0')} Mo/s</p>*/}
                                    {/*</div>*/}

                                    {/* Download buttons */}
                                    <div style={{textAlign: 'center', margin: 'auto'}} className="buttonsDownload">
                                        <IconButton
                                            style={{padding: '5px'}}
                                            disabled={currentDownload.downloadStatus.status !== 'paused'}
                                            onClick={() => resumeDownload(currentDownload)}
                                        >
                                            <PlayCircle/>
                                        </IconButton>

                                        <IconButton
                                            style={{padding: '5px'}}
                                            disabled={currentDownload.downloadStatus.status !== 'finishing' && currentDownload.downloadStatus.status !== 'extracting' && currentDownload.downloadStatus.status !== 'downloading'}
                                            onClick={() => pauseDownload(currentDownload)}
                                        >
                                            <PauseCircle/>
                                        </IconButton>

                                        <IconButton
                                            style={{padding: '5px'}}
                                            onClick={() => removeDownload(currentDownload)}
                                        >
                                            <RemoveCircle />
                                        </IconButton>
                                    </div>
                                </div>

                            </div>

                            <LinearProgress variant="determinate" value={Math.round(currentDownload.downloadStatus.sizeDownloaded*100 / currentDownload.downloadStatus.size)} />
                        </div>
                    )
                })
            }

        </div>
    )

}

export default UptoboxCurrentDownloads;
